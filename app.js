var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');

mongoose.connect('mongodb://dovydasre:amnestija@ds059524.mongolab.com:59524/pigeon');
require('./models/Users');
require('./models/Messages');
require('./config/passport');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

var http = require('http').Server(app);
var io = require('socket.io')(http);

// view engine setup
app.set('views', path.join(__dirname, 'public/views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());

// Socket.io

var Messages = require('./models/Messages').Messages;
var Users = require('./models/Users').Users;

var people = [];
io.on('connection', function (client) {
	
	client.on('join', function(name){
		
		console.info('New connection - ' + name, client.id);
		
		Users.update({username: name}, {status: 'online', socket_id: client.id}, function(){
			io.sockets.emit('update-users', {username: name, status: 'online', socket_id: client.id});
		});
	});
	
	client.on('disconnected', function(name){
		
		console.info('Disconnected - ' + name);
		
		Users.update({username: name}, {status: 'offline'}, function(){
			io.sockets.emit('update-users', {username: name, status: 'offline', socket_id: ''});
		});
	});

	client.on('send', function(msg){
		
		console.log('msg to: ' + msg.toSocketId);
		
		io.to( msg.toSocketId ).emit( 'chat', msg);
		
		var message = new Messages({
			body: msg.body,
			to: msg.to,
			from: msg.from,
			created_at: new Date().toISOString()
		});
		
		message.save(function (err){
			if(err) {
				return next(err);
			}
		});
	});
	
	client.on('typing', function(typingData){
		io.to( typingData.toSocketId ).emit( 'typing', typingData.from);
	});
	
	client.on('seen', function(seenData){
		io.to( seenData.toSocketId ).emit( 'seen', seenData);
	});
	
});

app.use('/', routes);
app.use('/users', users);

app.get('/*', function(req, res){
	res.render('index.ejs');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

http.listen(process.env.PORT || 8080);

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	
	app.use(function(err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			message: err.message,
			error: err
		});
	});
	
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {}
	});
});

module.exports = app;