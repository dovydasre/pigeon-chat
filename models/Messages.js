var mongoose = require('mongoose');

var MessageSchema = new mongoose.Schema({
	body: String,
	to: String,
	from: String,
	created_at: Date,
	seen: { type: Boolean, default: false }
});

var Messages = mongoose.model('Messages', MessageSchema);

module.exports = {
	Messages: Messages
}