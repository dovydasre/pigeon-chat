var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

var UserSchema = new mongoose.Schema({
	username: {type: String, lowercase: true, unique: true},
	firstname: String,
	lastname: String,
	fullname: String,
	status: { type: String, default: 'offline' },
	socket_id: { type: String, default: '' },
	hash: String,
	salt: String
});

UserSchema.methods.setPassword = function(password, update){
	this.salt = crypto.randomBytes(16).toString('hex');
	this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
	
	if (update) return {salt: this.salt, hash: this.hash};
};

UserSchema.methods.validPassword = function(password) {
	var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');

	return this.hash === hash;
};

UserSchema.methods.generateJWT = function() {
	var today = new Date();
	var exp = new Date(today);
	exp.setDate(today.getDate() + 30);

	return jwt.sign({
		_id: this._id,
		username: this.username,
		exp: parseInt(exp.getTime() / 1000),
	}, 'SECRET');
};

var Users = mongoose.model('User', UserSchema);

module.exports = {
	Users: Users
}




