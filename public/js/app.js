angular.module('pigeonChat', [
	'ui.router', 
	'btford.socket-io',
	'angularMoment',
	'ngSanitize',
	'ngResource',
	'angular.filter',
	'ngDialog',
	'ngAnimate',
	'smoothScroll'
])

.config(function($stateProvider, $locationProvider, $urlRouterProvider) {
	
	$stateProvider
		.state('app', {
			url: '/app',
			templateUrl: '/views/main.html',
			onEnter: ['$state', 'auth','messages', function($state, auth, messages){
				if(!auth.isLoggedIn()){
					$state.go('login');
				}
			}],
			resolve: {
				users: ['users', 'auth', function(users, auth){
					return users.getAll();
				}],
				messages: ['messages', function(messages){
					return messages.getAll();
				}]
			}
		})
		
		.state('app.users', {
			url: '/users/:username',
			templateUrl: '/views/partials/messenger.html',
			controller: 'ChatCtrl',
			resolve: {
				user: ['$stateParams', 'users', function($stateParams, users) {
					return users.data.filter(function(e){
						return e.username === $stateParams.username;
					});
				}]
			}
		})
		
		.state('login', {
			url: '/login',
			templateUrl: '/views/pages/login.html',
			controller: 'AuthCtrl',
			onEnter: ['$state', 'auth', function($state, auth){
				if(auth.isLoggedIn()){
					$state.go('app');
				}
			}]
		})
		
		.state('register', {
			url: '/register',
			templateUrl: '/views/pages/register.html',
			controller: 'AuthCtrl',
			onEnter: ['$state', 'auth', function($state, auth){
				if(auth.isLoggedIn()){
					$state.go('app');
				}
			}]
		});
	
	$urlRouterProvider.otherwise('app');
	$locationProvider.html5Mode(true);
})

.run(function(amMoment) {
	amMoment.changeLocale('lt');
})

.controller('MainCtrl', function($scope, $rootScope, $location, $timeout, users, socket, auth, messages, ngDialog) {
	$scope.users = users.users;
	$scope.messages = messages.messages;
	$scope.currentUser = auth.currentUser();
	$scope.connectedUsers = [];
	$scope.logOut = auth.logOut;
	$scope.searchContact = '';
	
	socket.connect();
	
	socket.emit('join', $scope.currentUser);
	
	socket.on('update-users', function(this_user){
		$scope.users.filter(function(e) {
			if (e.username === this_user.username) {
				e.status = this_user.status;
				e.socket_id = this_user.socket_id;
			}
		});
	});
	
	socket.on('chat', function(msg){
		$scope.messages.push(msg);
		$scope.sideBarStats();
	});
	
	window.onbeforeunload = function() {
		socket.emit('disconnected', $scope.currentUser);
	}
	
	$rootScope.$on('$locationChangeSuccess', function(next, current) {
		if ($location.path() === '/login' || $location.path() === '/register') {
			socket.emit('disconnected', $scope.currentUser);
		}
	});
	
	$scope.sideBarStats = function() {
		
		for (var i = $scope.users.length - 1; i >= 0; i--) {
			
			$scope.users[i].unseen = 0;
			var lastMessageIndex;
			
			for (var a = 0; a < $scope.messages.length; a++) {
				
				if ( $scope.messages[a].from === $scope.users[i].username ) {
					if ( $scope.messages[a].seen === false ) {
						$scope.users[i].unseen += 1;
					}
					
					if (typeof a !== 'undefined') {
						$scope.users[i].last_message = $scope.messages[a].body;
						$scope.users[i].last_message_date = $scope.messages[a].created_at;
					}
					
				}
			}
			
		};
	};
	
	$scope.userSettings = function () {
		ngDialog.open({
			template: '/views/partials/settings.html',
			controller: 'settingsCtrl',
			closeByNavigation: true
		});
	};
	
	$scope.sideBarStats();
	
})

.controller('ChatCtrl', function($scope, $timeout, $stateParams, user, socket, auth, messages){
	$scope.recipient;
	$scope.currentUser = auth.currentUser();
	$scope.userMessages = [];
	$scope.typing = false;
	
	for (var i = $scope.users.length - 1; i >= 0; i--) {
		if ($scope.users[i].username === user[0].username) $scope.recipient = $scope.users[i];
	}
	
	for (var i = 0; i < $scope.messages.length; i++) {
		if ($scope.messages[i].to === $scope.recipient.username || $scope.messages[i].from === $scope.recipient.username) {
			var day = $scope.messages[i].created_at.slice(0, 10);
			$scope.messages[i].created_at_day = day;
			$scope.userMessages.push($scope.messages[i]);
		}
	}
	
	$scope.sendMessage = function(body) {
		
		if ( body != null && body != '' ) {
			
			var targetSocket = $scope.users.filter(function(e){
				return e.username === $scope.recipient.username;
			});
			
			var date = new Date().toISOString();
			var msg = {
				body: body,
				to: $scope.recipient.username,
				from: $scope.currentUser,
				toSocketId: targetSocket.length ? targetSocket[0].socket_id : '',
				created_at: date,
				seen: false,
				created_at_day: date.slice(0, 10)
			};
			
			$scope.userMessages.push(msg);
			socket.emit('send', msg);
			$scope.body = '';
			
			var audio = document.querySelector('audio.send_msg_audio');
			audio.pause();
			audio.currentTime = 0;
			audio.play();
		}
	}

	socket.on('chat', function(msg){
		$scope.userMessages.push(msg);
		
		var audio = document.querySelector('new_msg_audio');
		audio.pause();
		audio.currentTime = 0;
		audio.play();
	});
	
	var typingTimer;
	socket.on('typing', function(username){
		if ($scope.recipient.username === username) {
			$scope.typing = true;
			
			$timeout.cancel(typingTimer);
			typingTimer = $timeout(function() {
				$scope.typing = false;
			}, 2000);
		}
	});
	
	socket.on('seen', function(data){
		$scope.userMessages.filter(function(e){
			if (e.from === data.to && e.to === data.from) {
				return e.seen = true;
			}
		});
	});
})

// .controller('HeaderCtrl', function($scope, auth){
// 	$scope.isLoggedIn = auth.isLoggedIn;
// 	$scope.currentUser = auth.currentUser;
// 	$scope.logOut = auth.logOut;
// })

.controller('settingsCtrl', function($scope, $element, $timeout, $http, auth, users){
	$scope.logOut = auth.logOut;
	$scope.saving = false;
	$scope.changePasswordActive = false;
	$scope.currentUser = users.users.filter(function(e){
		return e.username === auth.currentUser();
	})[0];
	$scope.newPassword = {
		oldPassword: '',
		newPassword: '',
		repeatedPassword: ''
	};
	
	$scope.changeFullname = function(e) {
		var parent = e.currentTarget;
		var input = $element[0].querySelector('[data-fullname]');
		
		parent.classList.add('active');
		input.removeAttribute('disabled');
		input.focus();
		
		input.addEventListener('blur', function(){
			input.setAttribute('disabled', '');
			parent.classList.remove('active');
		});
	};
	
	$scope.changePassword = function(close) {
		if (typeof close !== 'undefined') {
			$scope.changePasswordActive = false;
		} else {
			$scope.changePasswordActive = true;
		}
	}
	
	$scope.save = function() {
		$scope.saving = true;
		$scope.updateData = [];
		
		if (!$scope.changePasswordActive) {
			$scope.updateData.push({
				fullname: $scope.currentUser.fullname
			});
		} else {
			$scope.updateData.push($scope.newPassword);
		}
		
		$http.put('/users/' + $scope.currentUser.username, $scope.updateData).then(function(res){
			$scope.saving = false;
			$scope.errors = res.data.data;
			if (typeof $scope.errors === 'undefined') {
				$scope.saved = true;
				$timeout(function(){ $scope.saved = false; }, 3000);
			}
		});
	};
})


.controller('AuthCtrl',['$scope', '$state', 'auth', function($scope, $state, auth){
	$scope.user = {};
	
	$scope.register = function(){
		auth.register($scope.user).error(function(error){
			$scope.error = error;
		}).then(function(){
			$state.go('app');
		});
	};

	$scope.logIn = function(){
		auth.login($scope.user).error(function(error){
			$scope.error = error;
		}).then(function(){
			$state.go('app');
		});
	};
}])

.factory('users', function($http) {
	
	var o = {
		users: []
	};
	
	o.get = function(username) {
		return $http.get('/users/' + username).then(function(res) {
			return res.data;
		});
	};
	
	o.getAll = function() {
		return $http.get('/users').success(function(data){
			angular.copy(data, o.users);
		});
	};
	
	return o;
})

.factory('messages', function($http, auth){
	
	var m = {
		messages: []
	};
	
	m.getAll = function(username) {
		return $http.get('/messages/', {
			headers: {Authorization: 'Bearer ' + auth.getToken()}
		}).success(function(res){
			angular.copy(res, m.messages);
		});
	};
	
	return m;
})

.factory('auth', function($http, $window, $state) {
	
	var auth = {};
	
	auth.saveToken = function(token) {
		$window.localStorage['pigeon-chat-token'] = token;
	}
	
	auth.getToken = function() {
		return $window.localStorage['pigeon-chat-token'];
	}
	
	auth.isLoggedIn = function() {
		
		var token = auth.getToken();
		
		if (token) {
			var payload = JSON.parse($window.atob(token.split('.')[1]));
			
			return payload.exp > Date.now() / 1000;
		} else {
			return false;
		}
	}
	
	auth.currentUser = function(){
		
		if( auth.isLoggedIn() ){
			var token = auth.getToken();
			var payload = JSON.parse($window.atob(token.split('.')[1]));

			return payload.username;
		}
	};
	
	auth.register = function(user) {
		return $http.post('/register', user).success(function(data){
			auth.saveToken(data.token);
		});
	}
	
	auth.login = function(user) {
		return $http.post('/login', user).success(function(data){
			auth.saveToken(data.token);
		});
	}
	
	auth.logOut = function() {
		$window.localStorage.removeItem('pigeon-chat-token');
		$state.go('login');
	}
	
	return auth;
})

.factory('socket', function(socketFactory){
	return socketFactory();
})

.directive('messageContent', function($timeout, $stateParams, $http, socket) {
	return {
		restrict: 'A',
		require: 'ngModel',
		scope: false,
		link: function(scope, element, attrs, ngModel) {
			
			element.on('keydown', function(e) {
				if (e.keyCode == 13) {
					scope.sendMessage(ngModel.$$lastCommittedViewValue);
					window.event.cancelBubble = true;
					event.returnValue = false;
				} else {
					
					var targetSocket = scope.users.filter(function(e){
						return e.username === scope.recipient.username;
					});
					var typingData = {
						from: scope.currentUser,
						to: scope.recipient.username,
						toSocketId: targetSocket.length ? targetSocket[0].socket_id : ''
					};
					
					socket.emit('typing', typingData);
				}
			});
			
			function read() {
				ngModel.$setViewValue(element.html());
			}
			
			ngModel.$render = function() {
				element.html(ngModel.$viewValue || "");
			};
			
			element.bind('blur keyup change', function() {
				scope.$apply(read);
			});
			
			element.bind('focus', function(){
				
				for (var i = scope.users.length - 1; i >= 0; i--) {
					if (scope.users[i].username === $stateParams.username && scope.users[i].unseen > 0) {
						
						scope.users[i].unseen = 0;
						
						var targetSocket = scope.users.filter(function(e){
							return e.username === scope.recipient.username;
						});
						
						socket.emit('seen', {
							from: scope.currentUser,
							to: scope.recipient.username,
							toSocketId: targetSocket.length ? targetSocket[0].socket_id : ''
						});
						
						scope.messages.filter(function(e){
							if (e.from === scope.recipient.username && e.to === scope.currentUser) {
								return e.seen = true;
							}
						});
						
						var data = {
							to: scope.currentUser,
							from: $stateParams.username,
							seen: false
						};
						
						$http.put('/messages/seen/', data).then(function(res){
							
						});
					}
				};
			});
		}
	};
})

.directive('scrollbar', function($timeout, smoothScroll) {
	return {
		link: function(scope, element) {
			$timeout(function(){
				Ps.initialize(element[0]);
				element[0].scrollTop = element[0].querySelector('.messages').scrollHeight
			}, 0);

			scope.$watch(
				function(){
					var count = 0;
					var messagesGroup = element[0].querySelectorAll('.messages .messages_group');
					
					for (var i = messagesGroup.length - 1; i >= 0; i--) {
						count += messagesGroup[i].childNodes.length;
					}
					
					return count;
				},
				function (newValue, oldValue) {
					if (newValue !== oldValue) {
						$timeout(function(){
							Ps.update(element[0]);
							
							smoothScroll(element[0].querySelector('.messages'), {
								duration: 300,
								easing: 'linear',
								offset: -element[0].querySelector('.messages').scrollHeight,
								containerId: 'messages_holder'
							});
						}, 200);
					}
				}
			)
		}
	};
})

.directive('safePaste', [function() {
		
		var specialCharacters = ["–", "’"],
			normalCharacters = ["-", "'"]
		
		function replaceInvalidCharacters (string) {
			var regEx;
			
			for (var x = 0; x < specialCharacters.length; x++) {
				regEx = new RegExp(specialCharacters[x], 'g');
				string = string.replace(regEx, normalCharacters[x]).trim();
			}
			
			return string;
		}
		
		function handlePaste (event) {
			event.preventDefault();
			var plainText = event.clipboardData.getData('text/plain');
			var cleanText = replaceInvalidCharacters(plainText);
			document.execCommand('inserttext', false, cleanText);
			return false;
		}
		
		var declaration = {};
		declaration.restrict = 'A';
		
		declaration.link = function(scope, element, attr) {
			element.on('paste', handlePaste);
			scope.$on('$destroy', function() {
				element.off('paste', handlePaste);
			});
		};
		
		return declaration;
	}
]);