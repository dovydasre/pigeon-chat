var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var passport = require('passport');
var User = mongoose.model('User');
var Messages = mongoose.model('Messages');

var jwt = require('express-jwt');
var auth = jwt({secret: 'SECRET', userProperty: 'payload'});


router.post('/register', function(req, res, next) {
	
	if (!req.body.username || !req.body.password || !req.body.password_repeat || !req.body.firstname) {
		return res.status(400).json({
			username: {
				err: req.body.username ? false : true,
				message: 'Laukas yra privalomas'
			},
			password: {
				err: req.body.password ? false : true,
				message: 'Laukas yra privalomas'
			},
			password_repeat: {
				err: req.body.password_repeat ? false : true,
				message: 'Laukas yra privalomas'
			},
			firstname: {
				err: req.body.firstname ? false : true,
				message: 'Laukas yra privalomas'
			}
		});
	}
	
	if (!checkPassword(req.body.password)) {
		return res.status(400).json({
			password: {
				err: true,
				message: 'Slaptažodį turi sudaryti mažiausiai 6 simboliai, iš kurių turi būti mažiausiai viena didžioji raidė, mažoji raidė ir skaičius'
			}
		});
	} else if (req.body.username === req.body.password) {
		return res.status(400).json({
			password: {
				err: true,
				message: 'Slaptažodis negali sutapti su prisijungimo vardu'
			}
		});
	}
	
	if (req.body.password !== req.body.password_repeat ) {
		return res.status(400).json({
			password_repeat: {
				err: true,
				message: 'Slaptažodžiai nesutampa'
			},
			password: {
				err: true,
				message: 'Slaptažodžiai nesutampa'
			}
		})
	}

	var user = new User();
	user.username = req.body.username;
	user.firstname = req.body.firstname;
	user.lastname = req.body.lastname;
	user.fullname = req.body.lastname ? req.body.firstname + ' ' + req.body.lastname : req.body.firstname;
	user.setPassword(req.body.password);
	user.save(function (err){
		if (err) {
			if (err.code === 11000) {
				return res.status(500).json({
					username: {
						err: true,
						message: 'Toks vartotojo vardas jau egzistuoja'
					}
				});
			} else {
				return next(err);
			}
		}
		return res.json({token: user.generateJWT()});
	});
	
});

router.post('/login', function(req, res, next){
	
	if(!req.body.username || !req.body.password){
		return res.status(400).json({
			username: {
				err: req.body.username ? false : true,
				message: 'Prašome įvesti vartotojo vardą'
			},
			password: {
				err: req.body.password ? false : true,
				message: 'Prašome įvesti slaptažodį'
			}
		});
	}

	passport.authenticate('local', function(err, user, info){
		if (err) { return next(err); }
		if (user) {
			return res.json({token: user.generateJWT()});
		} else {
			return res.status(403).json({
				username: {
					err: true,
					message: 'Prašome patikrinti duomenis'
				},
				password: {
					err: true,
					message: 'Prašome patikrinti duomenis'
				}
			});
		}
	})(req, res, next);

});

router.get('/users', function(req, res, next) {
	
	User.find(function(err, users){
		if(err){ return next(err); }

		res.json(users);
	});
});

router.param('user', function(req, res, next, username) {
	
	var query = User.find({username: username});
	
	query.exec(function(err, user){
		if (err) { return next(err); }
		
		if ( !user ) { return next(new Error('nera')); }
		
		req.user = user;
		
		return next();
	});
});

router.get('/users/:user', function(req, res) {
	res.json(req.user);
});

router.put('/users/:user', function(req, res) {
	var username = {username: req.user[0].username};
	var data = req.body[0];
	console.log(data);
	
	if (data.fullname) {
		console.log(data.fullname);
	} else {
		var errors = false;
		Object.keys(data).forEach(function (key, index) {
			if (data[key] === '') {
				data[key] = {error: true, error_msg: 'Laukas privalomas'};
				errors = true;
			}
		});
		if (errors) return res.json({data});
		
		if (!req.user[0].validPassword(data.oldPassword)) {
			data.oldPassword = {error: true, error_msg: 'Slaptažodis neteisingas'};
			return res.json({data});
		}
		
		if (!checkPassword(data.newPassword)) {
			data.newPassword = {error: true, error_msg: 'Slaptažodį turi sudaryti mažiausiai 6 simbiliai, iš kurių turi būti mažiausiai viena didžioji raidė, mažoji raidė ir skaičius'};
			return res.json({data});
		}
		
		if (data.newPassword !== data.repeatedPassword) {
			data.newPassword = {error: true, error_msg: 'Slaptažodžiai nesutampa'};
			data.repeatedPassword = {error: true, error_msg: 'Slaptažodžiai nesutampa'};
			return res.json({data});
		}
		
		data = req.user[0].setPassword(data.newPassword, 'update');
	}
	
	User.update(username, data, function(err, num, raw) {
		if (err) return handleError(err);
		res.json();
	});
});

router.get('/messages/', auth, function(req, res) {
	var currentUser = req.payload.username;
	
	Messages.find({ $or: [{ to: currentUser}, { from: currentUser }] }, 
		function(err, data){
			res.json(data);
		});
});

router.put('/messages/seen/', function(req, res) {
	
	Messages.update(req.body, {seen: true}, {multi: true}, function(err, num, raw) {
		if (err) return handleError(err);
		res.json();
	});
});

router.use(function (err, req, res, next) {
	if (err.name === 'UnauthorizedError') {
		res.redirect('404.html');
	}
});

function checkPassword(str, username) {
	var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
	return re.test(str);
}

module.exports = router;