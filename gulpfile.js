"use strict";

var gulp 			= require( 'gulp' );
var sass 			= require( 'gulp-sass' );
var sourcemaps 		= require( 'gulp-sourcemaps' );
var autoprefixer 	= require( 'gulp-autoprefixer' );
var iconfont 		= require( 'gulp-iconfont' );
var spritesmith 	= require( 'gulp-spritesmith' );
var livereload 		= require( 'gulp-livereload' );
var consolidate 	= require( 'gulp-consolidate' );
var lodash  		= require( 'lodash' );
var watch 			= require( 'gulp-watch' );
var uglify 			= require( 'gulp-uglify' );
var concat 			= require( 'gulp-concat' );
var minifyCss 		= require( 'gulp-minify-css' );
var nodemon 		= require( 'gulp-nodemon' );

var config = {
	scssPath: 'scss/',
	cssPath: 'public/css/',
	jsPath: 'public/js/',
	htmlPath: 'public/views/',
	fontsPath: 'public/fonts/',
	uglify: {
		filename: 'all.min.js',
		manifest: 'manifest.json'
	}
};

gulp.task('sass', function () {
	
	console.log('sass');
	gulp.src( config.scssPath + '**/*.scss' )
		.pipe(sourcemaps.init())
		.pipe(sass({ outputStyle: 'nested' }).on('error', sass.logError))
		.pipe(autoprefixer('last 3 versions'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest( config.cssPath ))
		.pipe(livereload());
});

gulp.task('watch', function () {
	gulp.watch( config.scssPath + '**/*.scss' , ['sass']);
	
	gulp.src( config.htmlPath + '**/*.html', { read: false })
		.pipe(watch( config.htmlPath + '**/*.html' ))
		.pipe(livereload());
	
	gulp.src( config.jsPath + '**/*.js', { read: false })
		.pipe(watch( config.jsPath + '**/*.js' ))
		.pipe(livereload());
	
	livereload.listen();
});

gulp.task('demon', function () {
	console.log('default');
	nodemon({ 
		script: 'app.js',
		watch: ['app.js', 'routes/*.js'],
		ext: 'js'
	});
});

gulp.task('default', ['demon', 'watch']);

gulp.task('iconfont', function(){
	return gulp.src([ config.fontsPath + 'svg/**/*.svg'])
		.pipe(
			iconfont({
				fontName: 'icons',
				appendUnicode: false,
				normalize: true
			})
		)
		.on('glyphs', function(glyphs, options) {
			
			var unicodeGlyphs = [];
			
			for (var i = 0; i < glyphs.length; i++) {
				unicodeGlyphs.push({
					name: glyphs[i]['name'],
					unicode: glyphs[i].unicode[0].charCodeAt(0).toString(16).toUpperCase()
				});
			}
			
			gulp.src( config.fontsPath + 'templates/_icons.scss' )
			.pipe(consolidate('lodash', {
				glyphs: unicodeGlyphs,
				fontName: 'icons',
				fontPath: '../fonts/',
				className: 'icon'
			}))
			.pipe(gulp.dest( config.scssPath + 'core/' ));
		})
		.pipe(gulp.dest( config.fontsPath ));
});

gulp.task('compress', function() {
	
	var fs = require( 'fs' );
	
	var manifestFile = config.jsPath + config.uglify.manifest;
	
	if ( !fs.existsSync(manifestFile) ) {
		
		console.log('\x1b[31m', 'No manifest.json file found in ' + config.jsPath ,'\x1b[0m');
		return false;
	}
	
	var manifest = JSON.parse(fs.readFileSync(manifestFile, 'utf8'));
	
	gulp.src( manifest )
		.pipe(concat( config.uglify.filename ))
		.pipe(uglify({
			mangle: false
		}))
		.pipe(gulp.dest( config.jsPath ));
	
	console.log('');
	console.log('\x1b[32m', 'All js files were successfully minified, manifest: ' ,'\x1b[0m');
	console.log('\x1b[32m', manifest ,'\x1b[0m');
	console.log('');
});

gulp.task('minify-css', function() {
	return gulp.src(config.cssPath)
		.pipe(minifyCss({compatibility: 'ie10'}))
		.pipe(gulp.dest(config.cssPath));
});